<?php

/**
* helper function to nodeapi
* to turn an imce uploaded file into
* drupal file system file
* 1. use _filenode_check_imce_filepath() to 
*    check and get the realpath of the file
* 2. use _filenode_insert_file_with_realpath() to
*    actually insert the file into {file} table
* 3. return inserted file or FALSE
*/
function _filenode_imce_filepath_to_drupal_file($imce_filepath) {
  $file_directory_path = realpath(variable_get('file_directory_path', FALSE));
  $file_realpath = _filenode_check_imce_filepath($imce_filepath);
  if (($file_directory_path !== FALSE) && ($file_realpath !== FALSE)) {
    return _filenode_insert_file_with_realpath($file_realpath, $file_directory_path);
  }
  return FALSE;
}


/**
* helper function to nodeapi
* to turn an imce uploaded file into
* drupal file system file
*
* Case 1
* ------
* When uploaded, the input path would be the path of imce file
* relateive to base url. For example, /drupal-5/../../files/u1/foobar.doc
* we need to turn this filepath into a path relative to file upload directory
* For example, u1/foobar.doc
*
* Case 2
* ------
* The path could look like this sometimes:
* /drupal-5/system/files/u1/foobar.doc
* We need to turn this filepath into relative path like this:
* u1/foobar.doc
*
*/

function _filenode_check_imce_filepath($imce_filepath) {
  // change true path to a path relative to filesystem path
  $file_directory_path = variable_get('file_directory_path', 'sites/default/files');

  if ($file_directory_path!==FALSE) {

    $url_regex = sprintf("/^%s/", preg_quote(url(), "/"));    // regex to match drupal base url path
    $drupal_directory = dirname($_SERVER["SCRIPT_FILENAME"]); // dir base of drupal
    $file_directory_path = realpath($file_directory_path);    // get file directory path

    // get the drupal filesystem filepath
    $imce_relative_filepath = preg_replace($url_regex, "", $imce_filepath);

    // try to turn it into a realpath
    // this would also check if the file exists
    // if it is not there, would give out bool(false)
    $file_realpath = realpath("{$drupal_directory}/{$imce_relative_filepath}");

    //---------------------
    // Case 1: Public File
    //---------------------
    if ($file_realpath !== FALSE) {
      return $file_realpath;
    }
    
    //----------------------
    // Case 2: Private File
    //----------------------
    $url_regex = sprintf("/^%s/", preg_quote(url("system/files/"), "/"));  // regex to match drupal file system base url path
    if (preg_match($url_regex, $imce_filepath)) {
      $fs_relative_filepath = preg_replace($url_regex, "", $imce_filepath);
      $file_realpath = realpath(file_create_path("{$fs_relative_filepath}"));
      if ($file_realpath !== FALSE) {
        return $file_realpath;
      }
    }
  }
  
  return FALSE;
}


/**
* helper function of _filenode_imce_filepath_to_drupal_file
*/
function _filenode_drupal_file_exists($filepath) {
  $result = db_query("SELECT * FROM {files} WHERE filepath='%s'", $filepath);
  if ($file = db_fetch_object($result)) return $file;
  return FALSE;
}

/**
* helper function of _filenode_imce_filepath_to_drupal_file
*/
function _filenode_insert_file_with_realpath($file_realpath, $file_directory_path) {
  global $user;
  
  // even if the file is there, it may be out of file directory path
  // we need to counter possible hack here
  // so we need to check if it is inside file directory paty
  $file_dir_regex = sprintf("/^%s/", preg_quote("$file_directory_path/", "/"));
  if (preg_match($file_dir_regex, $file_realpath)){
    $filepath = preg_replace($file_dir_regex, "", $file_realpath);
    // check if the file does not exists in the database
    // if not, create a new record
    $file = _filenode_drupal_file_exists($filepath);
    if ($file == FALSE) {
      $fid = db_last_insert_id("file", "id");
      $filename = basename($file_realpath);
      $filemime = file_get_mimetype($filename);
      $filesize = filesize($file_realpath);
      db_query("INSERT INTO {files} 
      (fid, uid, filename, filepath, filemime, filesize, status, timestamp)
      VALUES (%d, %d, '%s', '%s', '%s', %d, %d, UNIX_TIMESTAMP())", 
      $fid, $user->uid, $filename, $filepath, $filemime, $filesize, 1); // use uid=1 for all of these uploads
      $file = _filenode_drupal_file_exists($filepath); // retrieve again the existing file
    }
    return $file; // give the existing file back to the system
  }
  return FALSE;
}



/**
* helper function to canonicalize url
*/
function _filenode_canonicalize_path($path) {
  // ./bloo ==> bloo
  $path = preg_replace('/^\.\//', '', $path);

  // bla/./bloo ==> bla/bloo
  $path = preg_replace('~/\./~', '/', $path);

  // resolve /../
  // loop through all the parts, popping whenever there's a .., pushing otherwise.
  $parts_before = explode('/', $path);
  foreach ($parts_before as $part) {
    if ($part === "..") {
      array_pop($parts);
    } else {
      $parts[] = $part;
    }
  }
  return implode("/", $parts);
}


